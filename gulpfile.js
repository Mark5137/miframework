var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano');

gulp.task('sass', function () {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass())
        .pipe(gulp.dest('dist/css'))
});

gulp.task('cssnano', function () {
    return gulp.src('dist/css/mi.css')
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('watch', ['sass', 'cssnano'], function () {
   gulp.watch('src/sass/**/*.sass', ['sass']);
   gulp.watch('dist/css/**/*.css', ['cssnano']);
});